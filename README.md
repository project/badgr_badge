Badgr Badges for Drupal 8
=================

Contents:
 * Introduction
 * History and Maintainers
 * Installation
 * Configuration

Introduction
------------

Badgr Badges is a Bridge between drupal and badgr.com api services. Badgr Badges are images that can be used to recognize skills and achievements. In general, badges are achieved through the demonstration of learning, the application or attainment of skills or by taking part in certain activities. However, Badgr Badges is a rich body of metadata that describes the criteria for achieving the badge, what the individual did to meet that criteria, who awarded the badge, and when it was awarded. Badgr Badges can be displayed online in a Bbadgr Backpack and then shared on websites, blogs and social networking sites such as LinkedIn.

Administrator can pull all his created badges from badgr.com. Once badges import done then admin can associate any content type with an entity reference field.

History and Maintainers
-----------------------

There are other contrib modules available which also provide the badge feature but this one is easy to implement.
Current Badgr Badges Maintainers:
 * Devendra Kumar Mishra

Installation
------------

Badgr Badges 8.x can be installed easily

1. Download the module to your DRUPAL_ROOT/modules/contribe/ directory or where ever you
install contrib modules on your site.
2. Go to Admin > Extend and enable the module.

Configuration
-------------

Configuration of Badgr Badges module.

1. Go to Administration > Configuration > System > Badgr Configuration
2. Fill Badgr Email and Badgr Password
3. Click "Save Configuration".
4. You should see 'Import badges' button to import all the badges from badgr account. Click on "Import badges" and check at /admin/content.

