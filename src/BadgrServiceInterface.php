<?php

namespace Drupal\badgr_badge;

/**
 * Interface BadgrServiceInterface.
 */
interface BadgrServiceInterface {

  /**
   * An badgr api call to generate token using user details
   *
   * @param array $_post_details
   *   User credentials.
   *
   * @return array
   *   Returns an badgr token value
   */
  public function badgr_initiate(array $_post_details);

  /**
   * Refreshing the token if the access token gets expired.
   *
   * @param string $refresh_token
   *   User credentials.
   *
   * @return array
   *   Returns an badgr token value
   */
  public function badgr_refresh_token(string $refresh_token);

  /**
   * Sets Badgr Header authorization
   *
   * @param string $access_token
   *   User credentials.
   *
   * @return array
   *   Returns an header value
   */
  function badgr_set_header(string $access_token);

  /**
   * To check whether the user is already authenticated.
   *
   * @param string $access_token
   *   User credentials.
   *
   * @return string
   *   Returns success/failure status
   */
  function badgr_user_authenticate(string $access_token);

  /**
   * To check whether the user is already authenticated.
   *
   * @param string $access_token
   *   User credentials.
   *
   * @param string $badgr_account_id
   *   Badgr account id.
   *
   * @return string
   *   Returns success/failure status
   */
  function reauthenticate_token_badgr_account(string &$access_token, string $badgr_account_id);

  /**
   * To check whether the user is already authenticated.
   *
   * @param string $access_token
   *   User credentials.
   *
   * @param string $_post_details
   *   Issue details.
   *
   * @param string $badgr_account_id
   *   Badgr account id.
   *
   * @return array
   *   Returns issuer object
   */
  function badgr_create_issuer(string $access_token, string $_post_details, string $badgr_account_id);

  /**
   * To read/update/delete an issuer on the badgr account
   *
   * @param string $access_token
   * @param string $entity_id
   * @param string|null $method
   *
   * @param array|null $_post_details
   * @param int $badgr_account_id
   *
   * @return mixed
   */
  function badgr_update_rud_issuer(string $access_token, string $entity_id, string $method = NULL, array  $_post_details = NULL, int $badgr_account_id);

  /**
   * To get list of issuer on the badgr account
   *
   * @param string $access_token
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_list_all_issuer(string &$access_token, int $badgr_account_id);

  /**
   * To get list of existing issuer on the badgr account
   *
   * @param string $access_token
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_list_all_existing_issuer(string &$access_token, int $badgr_account_id);

  /**
   * To create an based to the issuer on the badgr account
   *
   * @param string $access_token
   * @param string $_post_details
   * @param string $entity_id
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_create_issuer_badges(string &$access_token, string $_post_details, string $entity_id, int $badgr_account_id);

  /**
   * To get list of badges on the badgr account
   *
   * @param string $access_token
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_list_all_badges(string &$access_token, int $badgr_account_id);

  /**
   * To get list of existing badges on the badgr account
   *
   * @param string $access_token
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_list_all_existing_badges(string &$access_token, int $badgr_account_id);

  /**
   * To read/update/delete an issuer on the badgr account
   *
   * @param string $access_token
   * @param string $entity_id
   * @param string|null $method
   * @param array|null $_post_details
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_update_rud_badges(string $access_token, string $entity_id, string $method = NULL, array $_post_details = NULL, int $badgr_account_id);

  /**
   * Award badges on the badgr account
   *
   * @param string $access_token
   * @param array $_post_details
   * @param string $entity_id
   * @param int $badgr_account_id
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_award_badges(string &$access_token, string $_post_details, string $entity_id, int $badgr_account_id);

  /**
   * To get list of badges on the badgr account
   *
   * @param string $access_token
   * @param int $badgr_account_id
   * @param string $entity_id
   * @param string $recipient
   *
   * @return mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  function badgr_get_award_badges(string &$access_token, int $badgr_account_id, string $entity_id, string $recipient);
}
