<?php

namespace Drupal\badgr_badge;

use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class BadgrHelpers.
 */
class BadgrHelpers {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a BadgrHelpers object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Badgr class details
   *
   * @param array $details
   * @return array $_post_details
   */
  public static function _get_badgr_badges_classes_data(array $details) {
    $_post_details = [];
    if ($details) {
      $badge_image_uri = $details['badge_image'];
      $badge_image_url = file_get_contents(file_create_url($badge_image_uri));

      $_post_details = [
        'name' => $details['badge_name'],
        'image' => 'data:image/png;base64,' . base64_encode($badge_image_url),
        'description' => $details['badge_description'],
        'criteriaUrl' => $details['badge_earning_url'],
        'criteriaNarrative' => $details['badge_earning_criteria']
      ];
    }

    return $_post_details;
  }

  /**
   * Badgr isssuer details
   *
   * @param array $details
   * @return array $_post_details
   */
  public static function _get_badgr_issuer_classes_data(array $details) {
    $_post_details = [];
    if ($details) {
      $issuer_image_uri = $details['issuers_image'];
      $issuer_image_url = file_get_contents(file_create_url($issuer_image_uri));

      $_post_details = [
        'name' => $details['issuers_name'],
        'image' => 'data:image/png;base64,' . base64_encode($issuer_image_url),
        'description' => $details['issuers_description'],
        'email' => $details['badgr_account_email'],
        'url' => $details['issuer_website_url']
      ];
    }

    return $_post_details;
  }

  /**
   * Badgr class details
   *
   * @param array $acheived_date
   *  Credential Registration achivement date
   * @param Object $user
   *   Current user object
   * @return array $_post_details
   */
  public static function _get_badgr_badges_assertions_data($acheived_date, $user) {
    $_post_details = [];
    if ($acheived_date) {
      $_post_details = [
        'recipient' => [
          'identity' => $user->getEmail(),
          'type' => 'email',
          'hashed' => true,
          'plaintextIdentity' => 'mybadge!'
        ]
      ];
    }

    return $_post_details;
  }

  /**
  * Callback function to create issuers content
  * @param array $value_badges
  * @param int $badgerAccountID
  *
  * @return object $issuer_content
  */
  public static function _udl_badgr_create_badges_content(array $value_badges, int $badgerAccountID) {
    $issuer_content = FALSE;
    if (isset($value_badges['issuer']) && !empty($value_badges['issuer'])) {
      $issuer = NULL;
      $issuer = $this->entityTypeManager->getStorage('node')->loadByProperties(
        [
          'type' =>'badgr_issuer',
          'field_issuer_entity_id' => $value_badges['issuer']
        ]
      );
      $issuer = reset($issuer);
      if (is_object($issuer)) {
        if ($issuer->id()) {
          $issuer_content = $this->entityTypeManager->getStorage('node')->loadByProperties(
            [
              'type' =>'badgr_badges',
              'field_badge_entity_id' => $value_badges['entityId']
            ]
          );
          $issuer_content = reset($issuer_content);
          if (is_object($issuer_content)) {
            $issuer_content->set('title', $value_badges['name']);
            $issuer_content->set('body', $value_badges['description']);
            $issuer_content->set('field_badge_earning_criteria', $value_badges['criteriaNarrative']);
            $issuer_content->set('field_badge_earning_url', $value_badges['criteriaUrl']);
            $issuer_content->set('field_badge_account', ['target_id' => $badgerAccountID]);
            if (!empty($value_badges['image'])) {
              $image_file_name = pathinfo($value_badges['image']);
              $image = file_get_contents($value_badges['image']);
              $file = file_save_data($image, 'public://' . $image_file_name['basename'], FileSystemInterface::EXISTS_RENAME);
              $file = file_copy($file, 'public://');
              $issuer_content->set('field_badge_image',['target_id' => $file->id()]);
            }
            $issuer_content->save();
          }
          else {
            $issuer_content = Node::create(['type' => 'badgr_badges']);
            $issuer_content->set('title', $value_badges['name']);
            $issuer_content->set('body', $value_badges['description']);
            $issuer_content->set('field_badge_earning_criteria', $value_badges['criteriaNarrative']);
            $issuer_content->set('field_badge_earning_url', $value_badges['criteriaUrl']);
            $issuer_content->set('field_badge_account', ['target_id' => $badgerAccountID]);
            $issuer_content->set('field_badge_entity_id', $value_badges['entityId']);

            // Image insertion
            if (!empty($value_badges['image'])) {
              $image_file_name = pathinfo($value_badges['image']);
              $image = file_get_contents($value_badges['image']);
              $file = file_save_data($image, 'public://' . $image_file_name['basename'], FileSystemInterface::EXISTS_RENAME);
              if (!empty($file) && !empty($file->id())) {
                $file->setPermanent();
                $file->save();
                if (is_object($file)) {
                  $issuer_content->set('field_badge_image',
                    [
                      'target_id' => $file->id(),
                      'alt' => t('Badges logo'),
                      'uid' => 1,
                      'title' => $file->getFilename(),
                    ]
                  );
                }
              }
            }
            $issuer_content->enforceIsNew();
            $issuer_content->save();
          }
        }
        $issuer->field_badges_reference[] = ['target_id' => $issuer_content->id()];
        $issuer->save();
      }
    }
    return $issuer_content;
  }

  /**
   * Callback function to create issuers content
   *
   * @param array $value_badges
   * @param int $badgerAccountID
   */
  public static function _udl_badgr_create_issuers_content(array $value_issuer, int $badgerAccountID) {
    $issuer = FALSE;
    if ($value_issuer) {
      $issuer = $this->entityTypeManager->getStorage('node')->loadByProperties(
        [
          'type' =>'badgr_issuer',
          'field_issuer_entity_id' => $value_issuer['entityId']
        ]
      );
      $issuer = reset($issuer);
      if (is_object($issuer)) {
        if ($issuer->id()) {
          $issuer->set('title', $value_issuer['name']);
          $issuer->set('body', $value_issuer['description']);
          $issuer->set('field_issuer_contact_email', $value_issuer['email']);
          $issuer->set('field_issuer_website_url', $value_issuer['url']);
          $issuer->set('field_badge_account', ['target_id' => $badgerAccountID]);

          // Image insertion
          if (!empty($value_issuer['image'])) {
            $image_file_name = pathinfo($value_issuer['image']);
            $image = file_get_contents($value_issuer['image']);
            $file = file_save_data($image, 'public://' . $image_file_name['basename']);
            if (!empty($file) && !empty($file->id())) {
            $file->setPermanent();
            $file->save();
              if (is_object($file)) {
                $issuer->set('field_issuer_image',
                  [
                    'target_id' => $file->id(),
                    'alt' => t('Issuer logo'),
                    'uid' => 1,
                    'title' => $file->getFilename(),
                  ]
                );
              }
            }
          }
          $issuer->save();
        }
      }
      else {
        $issuer = Node::create(['type' => 'badgr_issuer']);
        $issuer->set('title', $value_issuer['name']);
        $issuer->set('body', $value_issuer['description']);
        $issuer->set('field_issuer_contact_email', $value_issuer['email']);
        $issuer->set('field_issuer_website_url', $value_issuer['url']);
        $issuer->set('field_badge_account', ['target_id' => $badgerAccountID]);
        $issuer->set('field_issuer_entity_id', $value_issuer['entityId']);
        // Image insertion
        if (!empty($value_issuer['image'])) {
          $image_file_name = pathinfo($value_issuer['image']);
          $image = file_get_contents($value_issuer['image']);
          $file = file_save_data($image, 'public://' . $image_file_name['basename'], FileSystemInterface::EXISTS_RENAME);
          if (!empty($file) && !empty($file->id())) {
            $file->setPermanent();
            $file->save();
            if (is_object($file)) {
              $issuer->set('field_issuer_image',
                [
                  'target_id' => $file->id(),
                  'alt' => t('Issuer logo'),
                  'uid' => 1,
                  'title' => $file->getFilename(),
                ]
              );
            }
          }
        }
        $issuer->enforceIsNew();
        $issuer->save();
      }
    }
    return $issuer;
  }
}
