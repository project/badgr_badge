<?php

namespace Drupal\badgr_badge\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\badgr_badge\BadgrServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\badgr_badge\BadgrHelpers;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller for badgr backpack.
 */
class AddToBackpack extends ControllerBase {

  /**
   * The current user service.
   *
   * @var use Drupal\Core\Session\AccountInterface;
   */
  protected $currentUser;

  /**
   * Drupal\badgr_badge\BadgrServiceInterface definition.
   *
   * @var \Drupal\badgr_badge\BadgrServiceInterface
   */
  protected $badgrService;


  /**
   * Constructs a \Drupal\badgr_badge\Form object.
   *
   * @param \Drupal\badgr_badge\BadgrServiceInterface $badgrService
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(BadgrServiceInterface $badgrService, AccountInterface $current_user) {
    $this->badgrService = $badgrService;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('badgr_badge.service'),
      $container->get('current_user')
    );
  }

  /**
   * Callback to backpack.
   *
   * @param Object $badge
   *   Object of content type Badgr Badges
   * @param string $acheived_date
   *   timestamp when credential Registration status is Achieved
   *
   * @return string
   */
  public function addtobackpack(NodeInterface $badge, string $acheived_date, $nojs, Request $request) {
    if (is_object($badge)) {
      $badge_entity_id = NULL;
      $badgr_account = NULL;
      $output = NULL;
      $user = $this->currentUser;

      if (!$badge->get('field_badge_entity_id')->isEmpty()) {
        $badge_entity = $badge->get('field_badge_entity_id')->getValue()[0]['value'];
      }

      if (!$badge->get('field_badge_image')->isEmpty()) {
        $badge_image = file_create_url($badge->field_badge_image->entity->getFileUri());
      }

      if (!$badge->get('field_badge_account')->isEmpty()) {
        $badgr_account = $badge->get('field_badge_account')->first()->get('entity')->getTarget()->getValue();
        if (is_object($badgr_account)) {
          if (!$badgr_account->get('field_badgr_access_token')->isEmpty()) {
            $access_token = $badgr_account->get('field_badgr_access_token')->getValue()[0]['value'];
            $award_data = $this->badgrService->badgr_get_award_badges($access_token, $badgr_account->id(), $badge_entity, $user->getEmail());
            try {
              if($award_data['status']['success']) {
                if(empty($award_data['result'])) {
                  $_post_details = Json::encode(BadgrHelpers::_get_badgr_badges_assertions_data($acheived_date, $user));
                  $output = $this->badgrService->badgr_award_badges($access_token, $_post_details, $badge_entity, $badgr_account->id());
                  if(!empty($output['result'])) {
                    $status_message = 'Congratulations! Your ' . $badge->label() . ' Badge is added to your Badgr backpack.';
                    $this->messenger()->addMessage($this->t($status_message), 'status');
                  }
                }
                else {
                  $status_message = 'Your ' . $badge->label() . ' Badge is already added to your Badgr backpack.';
                  $this->messenger()->addMessage($this->t($status_message), 'status');
                }
              }
            }
            catch (RequestException $e) {
              //An error happened.
              if ($e->hasResponse()) {
                watchdog_exception('badgr_badge', $e->getMessage());
                $status_message = $e->getMessage();
              }
            }
          }
        }
      }
    }
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#backpack{$badge->id()}", $status_message));
    return $response;
  }
}
