<?php

namespace Drupal\badgr_badge\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\File\FileSystemInterface;
use Drupal\badgr_badge\BadgrServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\badgr_badge\BadgrHelpers;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class BadgrConfigForm.
 */
class BadgrConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The badgr service manager.
   *
   * @var \Drupal\badgr_badge\BadgrServiceInterface
   */
  protected $badgrService;

  /**
   * Constructs a \Drupal\badgr_badge\Form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\badgr_badge\BadgrServiceInterface $badgrService
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, BadgrServiceInterface $badgrService) {
    $this->entityTypeManager = $entity_type_manager;
    $this->badgrService = $badgrService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('badgr_badge.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'badgr_badge.badgrconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'badgr_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('badgr_badge.badgrconfig');
    $form['badgr_into'] = [
      '#type' => 'item',
      '#markup' => $this->t('This form is used to fetch issuers & badges from badgr.com account.'),
    ];
    $form['badgr_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Badgr Email'),
      '#description' => $this->t('Email Address of the live badgr account'),
      '#default_value' => $config->get('badgr_email'),
    ];
    $form['badgr_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Badgr Password'),
      '#description' => $this->t('Password of the live badgr account'),
      '#default_value' => $config->get('badgr_password'),
    ];
    if (!empty($config->get('badgr_email')) && !empty($config->get('badgr_password'))) {
      $form['actions']['import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import badges'),
        '#button_type' => 'primary',
        '#weight' => 99
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $op = $form_state->getValue('op')->__toString();

    if ($op == 'Save configuration') {
      $this->config('badgr_badge.badgrconfig')
        ->set('badgr_email', $form_state->getValue('badgr_email'))
        ->set('badgr_password', $form_state->getValue('badgr_password'))
        ->save();
    }
    else {
      $badge_account = $this->entityTypeManager->getStorage('node')->loadByProperties(
        [
          'type' =>'badgr_account',
          'field_badgr_identifier' => 1]
        );
      $badge_account = reset($badge_account);
      if (is_object($badge_account)) {
        $badgr_access_token = $badge_account->get('field_badgr_access_token')->getValue()[0]['value'];
        // Fetch all issuers from badgr
        $_all_issuers = $this->badgrService->badgr_list_all_issuer($badgr_access_token, $badge_account->id());
        // Insert / Update Issuer in the learning designed.
        if (!$_all_issuers['status']['success']) {
          $this->messenger()->addError('There was problem fetching the Issuers. Please try again');
          return FALSE;
        }
        foreach ($_all_issuers['result'] as $key_issuer => $value_issuer) {
          BadgrHelpers::_udl_badgr_create_issuers_content($value_issuer, $badge_account->id());
        }

        // Fetch all badges associated with badges
        $_all_badges = $this->badgrService->badgr_list_all_badges($badgr_access_token, $badge_account->id());
        if (!$_all_badges['status']['success']) {
          $this->messenger()->addError('There was problem fetching the Badgess. Please try again', 'error');
          return FALSE;
        }
        foreach ($_all_badges['result'] as $key_badges => $value_badges) {
          BadgrHelpers::_udl_badgr_create_badges_content($value_badges, $badge_account->id());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $op = $form_state->getValue('op')->__toString();

    if ($op == 'Save configuration') {
      $_post_details = [
        'username' => $form_state->getValue('badgr_email'),
        'password' => $form_state->getValue('badgr_password')
      ];

      $badgr = $this->badgrService->badgr_initiate($_post_details);
      if (isset($badgr['error'])) {
        $form_state->setErrorByName('badgr_email', $this->t($badgr['error_description']));
      }

      if (isset($badgr['access_token'])) {
        $badge_account = $this->entityTypeManager->getStorage('node')->loadByProperties(
        [
          'type' =>'badgr_account',
          'field_badgr_identifier' => 1]
        );
        $badge_account = reset($badge_account);
        if (is_object($badge_account)) {
          $badge_account->set('field_badgr_access_token', $badgr['access_token']);
          $badge_account->set('field_badgr_refresh_token', $badgr['refresh_token']);
          $badge_account->save();
        }
        else {
          $article = Node::create(['type' => 'badgr_account']);
          $article->set('title', 'Learning Designed');
          $article->set('field_badgr_access_token', $badgr['access_token']);
          $article->set('field_badgr_refresh_token', $badgr['refresh_token']);
          $article->set('field_badgr_identifier', 1);
          $article->set('field_badgr_email', $form_state->getValue('badgr_email'));
          $article->enforceIsNew();
          $article->save();
        }
      }
    }
  }
}
